import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Chart} from "chart.js";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'naelGG';
  account: any;
  puuid: any;
  APIKey = '';
  cursor = 0;
  matchData: any;
  timeline: any;
  lastMatches: string[];
  requestGetPUUID = 'https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/';
  requestGetMatches = 'https://europe.api.riotgames.com/lol/match/v5/matches/by-puuid/';
  requestGetMatchData = 'https://europe.api.riotgames.com/lol/match/v5/matches/';
  summonerName = '';
  headers: any;
  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {

  }

  onSubmit(summonerName: any): void{
    console.log(summonerName);
    this.summonerName = encodeURI(summonerName.summonerName);
    this.APIKey = summonerName.APIKey;
    this. headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'X-Riot-Token': summonerName.APIKey
    });
    console.log(this.APIKey);
    this.getPUUID(this.summonerName).then(() => {
      this.getMatchesByPUUID(this.puuid).then(() => {
        this.getMatchData(this.lastMatches[0]).then(() => {
          console.log(this.matchData);
          this.getMatchTimeline(this.lastMatches[0]).then(() => {
            console.log(this.timeline);
            this.buildGoldGraph();
            this.buildCSGraph();
          });
        });
      });
    });
  }
  getPUUID(summonerName: string): Promise<any> {
    return new Promise((resolve) => {
      this.http.get<any>(this.requestGetPUUID + summonerName, {headers: this.headers}).subscribe(data => {
        this.puuid = data.puuid;
        resolve();
      });
    });
  }
  getMatchesByPUUID(puuid: string): Promise<any> {
    return new Promise((resolve) => {
      this.http.get<any>(this.requestGetMatches + puuid + '/ids?start=0&count=20', {headers: this.headers}).subscribe( data => {
        this.lastMatches = data;
        resolve();
      });
    });
  }
  getMatchData(match: string): Promise<any> {
    return new Promise((resolve) => {
      this.http.get<any>(this.requestGetMatchData + match, {headers: this.headers}).subscribe(data => {
        this.matchData = data;
        resolve();
      });
    });
  }

  getMatchTimeline(match: string): Promise<any> {
    return new Promise((resolve) => {
      this.http.get<any>(this.requestGetMatchData + match + '/timeline', {headers: this.headers}).subscribe(data => {
        this.timeline = data;
        resolve();
      });
    });
  }

  computeKDA(kills: any, deaths: any, assists: any): number {
    if (deaths === 0){
      deaths = 1;
    }
    return (kills + assists) / deaths;
  }

  computedSpentGold(goldEarned: any, goldSpent: any): number {
    return (goldSpent / goldEarned) * 100;
  }
  counter(i: number): any {
    return new Array(i);
  }

  updateTimeline(value): void {
    this.cursor = value.target.value;
  }

  getDiff(blue: number, red: number): string {
    let result = '';
    if (blue >= red){
      result = '< ' + (blue - red);
    } else {
      result = (red - blue) + ' >';
    }
    return result;
  }

  private buildGoldGraph(): void {
    const canvas = document.getElementById('goldGraph') as HTMLCanvasElement;
    const context = canvas.getContext('2d');
    const myChart = new Chart(context, {
      type: 'line',
      data: {
        labels: Array.from({length: this.timeline.info.frames.length}, (v, k) => k + 1),
        datasets: [
          {
          label: this.matchData.info.participants[0].championName,
          data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[1].totalGold),
          backgroundColor: [
            'rgba(52, 152, 219,0.4)'
          ],
          borderColor: [
            'rgba(41, 128, 185,1.0)'
          ],
          borderWidth: 1
        },
          {
            label: this.matchData.info.participants[1].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[2].totalGold),
            backgroundColor: [
              'rgba(52, 152, 219,0.4)'
            ],
            borderColor: [
              'rgba(41, 128, 185,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[2].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[3].totalGold),
            backgroundColor: [
              'rgba(52, 152, 219,0.4)'
            ],
            borderColor: [
              'rgba(41, 128, 185,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[3].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[4].totalGold),
            backgroundColor: [
              'rgba(52, 152, 219,0.4)'
            ],
            borderColor: [
              'rgba(41, 128, 185,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[4].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[5].totalGold),
            backgroundColor: [
              'rgba(52, 152, 219,0.4)'
            ],
            borderColor: [
              'rgba(41, 128, 185,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[5].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[6].totalGold),
            backgroundColor: [
              'rgba(231, 76, 60,0.4)'
            ],
            borderColor: [
              'rgba(192, 57, 43,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[6].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[7].totalGold),
            backgroundColor: [
              'rgba(231, 76, 60,0.4)'
            ],
            borderColor: [
              'rgba(192, 57, 43,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[7].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[8].totalGold),
            backgroundColor: [
              'rgba(231, 76, 60,0.4)'
            ],
            borderColor: [
              'rgba(192, 57, 43,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[8].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[9].totalGold),
            backgroundColor: [
              'rgba(231, 76, 60,0.4)'
            ],
            borderColor: [
              'rgba(192, 57, 43,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[9].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[10].totalGold),
            backgroundColor: [
              'rgba(231, 76, 60,0.4)'
            ],
            borderColor: [
              'rgba(192, 57, 43,1.0)'
            ],
            borderWidth: 1
          }

        ]
      },
      options: {
        scales: {
          y: {
            beginAtZero: false
          }
        }
      }
    });
  }

  private buildCSGraph(): void {
    const canvas = document.getElementById('csGraph') as HTMLCanvasElement;
    const context = canvas.getContext('2d');
    const myChart = new Chart(context, {
      type: 'line',
      data: {
        labels: Array.from({length: this.timeline.info.frames.length}, (v, k) => k + 1),
        datasets: [
          {
            label: this.matchData.info.participants[0].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[1].minionsKilled),
            backgroundColor: [
              'rgba(52, 152, 219,0.4)'
            ],
            borderColor: [
              'rgba(41, 128, 185,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[1].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[2].jungleMinionsKilled),
            backgroundColor: [
              'rgba(52, 152, 219,0.4)'
            ],
            borderColor: [
              'rgba(41, 128, 185,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[2].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[3].minionsKilled),
            backgroundColor: [
              'rgba(52, 152, 219,0.4)'
            ],
            borderColor: [
              'rgba(41, 128, 185,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[3].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[4].minionsKilled),
            backgroundColor: [
              'rgba(52, 152, 219,0.4)'
            ],
            borderColor: [
              'rgba(41, 128, 185,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[4].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[5].minionsKilled),
            backgroundColor: [
              'rgba(52, 152, 219,0.4)'
            ],
            borderColor: [
              'rgba(41, 128, 185,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[5].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[6].minionsKilled),
            backgroundColor: [
              'rgba(231, 76, 60,0.4)'
            ],
            borderColor: [
              'rgba(192, 57, 43,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[6].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[7].jungleMinionsKilled),
            backgroundColor: [
              'rgba(231, 76, 60,0.4)'
            ],
            borderColor: [
              'rgba(192, 57, 43,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[7].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[8].minionsKilled),
            backgroundColor: [
              'rgba(231, 76, 60,0.4)'
            ],
            borderColor: [
              'rgba(192, 57, 43,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[8].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[9].minionsKilled),
            backgroundColor: [
              'rgba(231, 76, 60,0.4)'
            ],
            borderColor: [
              'rgba(192, 57, 43,1.0)'
            ],
            borderWidth: 1
          },
          {
            label: this.matchData.info.participants[9].championName,
            data: Array.from({length: this.timeline.info.frames.length}, (v, k) => this.timeline.info.frames[k].participantFrames[10].minionsKilled),
            backgroundColor: [
              'rgba(231, 76, 60,0.4)'
            ],
            borderColor: [
              'rgba(192, 57, 43,1.0)'
            ],
            borderWidth: 1
          }

        ]
      },
      options: {
        scales: {
          y: {
            beginAtZero: false
          }
        }
      }
    });
  }
}
