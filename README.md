# NaelGG

A small project with Riot API. For personal use only.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.2.

## Installation

1. Clone or download the project from the repository.
2. Get your latest node version [here](https://nodejs.org/en/) (I use v16.15.0).
3. Disable the CORS protocol from your Chrome browser (see section below).
4. Get your free Riot API key [here](https://developer.riotgames.com) (you will need a Riot account). Notice that the API key lasts 24 hours after generating it but can be regenerated for free.
5. Navigate through the root of the project repository, run `ng serve` to launch the project, then open your CORS-free Chrome browser and navigate to `http://localhost:4200/`.

## Features

Nael.GG offers some features giving you some key data from your last game.

### Game final state
![Game final state](src/assets/img/main.png)

First table shows the state of the game after its ending :
- Victorious side
- Players and champions involved
- Score of each players with KDA
- Items bought
- Economy resume
- Vision score
- Dragon and baron kills
- Some miscellaneous events

### Game timeline

![Timeline](src/assets/img/timeline.png)

Use the range input to see the gold difference, CS difference and level difference of each match-up at each minute of the game.

### Gold and CS charts

![Graphs](src/assets/img/graphs.png)

Observe the gold and CS difference of each match-up with a global view with these charts, using [Chart.js](https://www.chartjs.org/)

## Disable Chrome CORS protocol.

Client-side calls to the Riot API are blocked because there is no way to make them without exposing your API key to users. 

As this project is only intended for personal usage, you can modify your browser settings to disable CORS (the mechanism which blocks you from making client-side calls).

To do that, create a shortcut of your Chrome browser. Open the properties of the shortcut and add `--disable-web-security --user-data-dir="D:/Chrome"` after `your\path\to\chrome.exe`.

Apply and close.

